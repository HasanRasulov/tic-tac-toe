#-------------------------------------------------
#
# Project created by QtCreator 2019-11-29T23:24:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Tic-Tac-Toe
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        Util.cpp

HEADERS  += mainwindow.h\
            Util.h

FORMS    +=
