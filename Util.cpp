#include "Util.h"

bool Util::can_move(const Board &board) {


    return std::any_of(board.begin(), board.end(), [](std::array<Player,cells> arr) {
        return std::any_of(arr.begin(), arr.end(), [](Player p) { return p == Player::Empty; });
    });
}

Result Util::who_is_winner(const Board &board) {


    for (size_t i = 0; i < cells; ++i) {

        if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][2] == board[i][3]) {
            if (board[i][0] == Player::X) return Result::X;
            else if (board[i][0] == Player::O) return Result::Y;
        }

    }

    for (size_t i = 0; i < cells; ++i) {
        if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[2][i] == board[3][i]) {
            if (board[i][0] == Player::X) return Result::X;
            else if (board[i][0] == Player::O) return Result::Y;
        }
    }

    if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[2][2] == board[3][3]) {
        if (board[0][0] == Player::X) return Result::X;
        else if (board[0][0] == Player::O) return Result::Y;
    }

    if (board[0][3] == board[1][2] && board[1][2] == board[2][1] && board[2][1] == board[3][0]) {
        if (board[0][3] == Player::X) return Result::X;
        else if (board[0][3] == Player::O) return Result::Y;
    }

    return Result::equal;

}

std::pair<int, int> Util::find_optimal_position(Board &board) {


    Result rs = Result::min;
    std::pair<int, int> res{-1, -1};

    for (size_t i = 0; i < cells; ++i) {
        for (size_t j = 0; j < cells; ++j) {

            if (board[i][j] == Player::Empty) {
                board[i][j] = Player::X;
                Result move = minimax(board, 0, false);
                //if(move==Result::equal)
                board[i][j] = Player::Empty;

                if (move > rs) {
                    res.first = i;
                    res.second = j;
                    rs = move;
                }
            }

        }
    }

    return res;
}


Result Util::minimax(Board &board, size_t depth, bool is_max) {

    Result res = who_is_winner(board);

    if (res == Result::X) return Result::X;
    if (res == Result::Y) return Result::Y;

    if (!can_move(board)) return Result::equal;

    if (is_max) {

        Result max = Result::min;

        for (size_t i = 0; i < cells; ++i) {
            for (size_t j = 0; j < cells; ++j) {

                if (board[i][j] == Player::Empty) {

                    board[i][j] = Player::X;
                    max = std::max(max, minimax(board, depth + 1, !is_max));
                    board[i][j] = Player::Empty;
                }

            }
        }

        return max;

    } else {

        Result min = Result::max;

        for (size_t i = 0; i < cells; ++i) {
            for (size_t j = 0; j < cells; ++j) {

                if (board[i][j] == Player::Empty) {
                    board[i][j] = Player::O;
                    min = std::min(min, minimax(board, depth + 1, !is_max));
                    board[i][j] = Player::Empty;
                }

            }
        }

        return min;

    }


}

void Util::print_board(const Board &board) {

    std::cout << "--------------\n";

    for (size_t i = 0; i < board.size(); ++i) {
        std::cout << "--";
        for (size_t j = 0; j < board[i].size(); ++j) {


            std::cout << (char) board[i][j] << "--";
        }
        std::cout << std::endl;
    }

    std::cout << "--------------";

}


