#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QVector>
#include <QPair>
#include<QLabel>
#include<QHBoxLayout>
#include<QThread>
#include <array>
#include <algorithm>
#include "Util.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QDialog {
  Q_OBJECT

public:
  explicit MainWindow(size_t cell_size =40,QWidget *parent = 0);
  ~MainWindow();

  virtual void paintEvent(QPaintEvent *);
  void mousePressEvent(QMouseEvent *);

  void print_board() const;

  QPair<int, int> find_optimal_position();

private:
  Ui::MainWindow *ui;
  QVector<QPair<size_t, size_t>> vec_X;
  QVector<QPair<size_t, size_t>> vec_O;
  size_t cell_size;
  Board board;

  bool can_move() const;

  Result who_is_winner() const;

  Result minimax(size_t depth, bool is_max);

};

#endif // MAINWINDOW_H
