#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(size_t cell_size,QWidget *parent)
    : QDialog(parent), ui(new Ui::MainWindow),cell_size{cell_size} {

  this->setFixedHeight(cells * cell_size);
  this->setFixedWidth(cells * cell_size);

  for (size_t i = 0; i < cells; i++)
    for (size_t j = 0; j < cells; j++)
      this->board[i][j] = Player::Empty;



  board = {{{{Player::X, Player::Empty, Player::Empty, Player::X}},
            {{Player::O, Player::X, Player::X, Player::O}},
            {{Player::O, Player::X, Player::Empty, Player::O}},
            {{Player::O, Player::Empty, Player::X, Player::Empty}}}};

}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::paintEvent(QPaintEvent *event) {

  QPainter painter(this);

  QPen pen;
  pen.setColor(Qt::white);
  pen.setWidth(5);
  painter.setPen(pen);

  painter.drawRect(QRect(0, 0, cells * cell_size, cells * cell_size));

  for (size_t i = 1; i <= cells; i++)
    painter.drawLine(0, cell_size * i, cell_size * cells, cell_size * i);
  for (size_t i = 1; i <= cells; i++)
    painter.drawLine(cell_size * i, 0, cell_size * i, cell_size * cells);

  for (int i = 0; i < vec_O.size(); i++) {

    size_t c_x = vec_O[i].first;
    size_t c_y = vec_O[i].second;

    painter.drawEllipse(QPoint(c_x, c_y), cell_size / 3, cell_size / 3);
  }

  for (int i = 0; i < vec_X.size(); i++) {
    size_t l_x = vec_X[i].first;
    size_t l_y = vec_X[i].second;
    painter.drawLine(l_x, l_y, l_x - cell_size, l_y - cell_size);
    painter.drawLine(l_x, l_y - cell_size, l_x - cell_size, l_y);
  }
}


void MainWindow::mousePressEvent(QMouseEvent *event) {

  if (event->buttons() == Qt::LeftButton) {

    size_t x = event->x();
    size_t y = event->y();

    size_t i = y / cell_size;
    size_t j = x / cell_size;

    if (board[i][j] == Player::Empty) {

      Result res = who_is_winner();

      if (res == Result::X) {
        qDebug() << "X is winner";
        this->setDisabled(true);
        this->close();
        return;
      } else if (res == Result::Y) {
        qDebug() << "Y is winner";
        this->setDisabled(true);
        this->close();
        return;
      }

      board[i][j] = Player::O;


      size_t c_x = cell_size - x % cell_size + x;
      size_t c_y = cell_size - y % cell_size + y;

      vec_O.push_back({
          c_x - cell_size / 2, c_y - cell_size / 2,
      });

      update();

      auto pos = find_optimal_position();

      if (pos == QPair<int, int>{-1, -1}){
        qDebug()<<"None of them is winner\n";
        this->setDisabled(true);
        return;
      }

      board[pos.first][pos.second] = Player::X;

      vec_X.push_back(
          {(pos.first + 1) * cell_size, (pos.second + 1) * cell_size});

      update();

      res = who_is_winner();

      if (res == Result::X) {
        qDebug() << "X is winner";
        this->setDisabled(true);
        this->close();
        return;
      } else if (res == Result::Y) {
        qDebug() << "Y is winner";
        this->setDisabled(true);
        this->close();
        return;
      }
    }
  }
}

bool MainWindow::can_move() const {

  return std::any_of(
      board.begin(), board.end(), [](std::array<Player, cells> arr) {
        return std::any_of(arr.begin(), arr.end(),
                           [](Player p) { return p == Player::Empty; });
      });
}

Result MainWindow::who_is_winner() const {

  for (size_t i = 0; i < cells; ++i) {

    if (board[i][0] == board[i][1] && board[i][1] == board[i][2] &&
        board[i][2] == board[i][3]) {
      if (board[i][0] == Player::X)
        return Result::X;
      else if (board[i][0] == Player::O)
        return Result::Y;
    }
  }

  for (size_t i = 0; i < cells; ++i) {
    if (board[0][i] == board[1][i] && board[1][i] == board[2][i] &&
        board[2][i] == board[3][i]) {
      if (board[0][i] == Player::X)
        return Result::X;
      else if (board[0][i] == Player::O)
        return Result::Y;
    }
  }

  if (board[0][0] == board[1][1] && board[1][1] == board[2][2] &&
      board[2][2] == board[3][3]) {
    if (board[0][0] == Player::X)
      return Result::X;
    else if (board[0][0] == Player::O)
      return Result::Y;
  }

  if (board[0][3] == board[1][2] && board[1][2] == board[2][1] &&
      board[2][1] == board[3][0]) {
    if (board[0][3] == Player::X)
      return Result::X;
    else if (board[0][3] == Player::O)
      return Result::Y;
  }

  return Result::equal;
}

QPair<int, int> MainWindow::find_optimal_position() {

  Result rs = Result::min;
  QPair<int, int> res{-1, -1};

  for (size_t i = 0; i < cells; ++i) {
    for (size_t j = 0; j < cells; ++j) {

      if (board[i][j] == Player::Empty) {
        board[i][j] = Player::X;
        Result move = minimax(0, false);

        board[i][j] = Player::Empty;

        if (move > rs) {
          res.first = i;
          res.second = j;
          rs = move;
        }
      }
    }
  }

  return res;
}

Result MainWindow::minimax(size_t depth, bool is_max) {

  Result res = who_is_winner();

  if (res == Result::X)
    return Result::X;
  if (res == Result::Y)
    return Result::Y;

  if (!can_move())
    return Result::equal;

  if (is_max) {

    Result max = Result::min;

    for (size_t i = 0; i < cells; ++i) {
      for (size_t j = 0; j < cells; ++j) {

        if (board[i][j] == Player::Empty) {

          board[i][j] = Player::X;
          max = std::max(max, minimax(depth + 1, !is_max));
          board[i][j] = Player::Empty;
        }
      }
    }

    return max;

  } else {

    Result min = Result::max;

    for (size_t i = 0; i < cells; ++i) {
      for (size_t j = 0; j < cells; ++j) {

        if (board[i][j] == Player::Empty) {
          board[i][j] = Player::O;
          min = std::min(min, minimax(depth + 1, !is_max));
          board[i][j] = Player::Empty;
        }
      }
    }

    return min;
  }
}

void MainWindow::print_board() const {

  qDebug() << "--------------\n";

  for (size_t i = 0; i < board.size(); ++i) {
    qDebug() << "--";
    for (size_t j = 0; j < board[i].size(); ++j) {

      qDebug() << (char)board[i][j] << "--";
    }
    qDebug() << "\n";
  }

  qDebug() << "--------------";
}
