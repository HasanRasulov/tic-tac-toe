//
// Created by hasan on 11/20/19.
//

#ifndef UNTITLED_UTIL_H
#define UNTITLED_UTIL_H

#include <array>
#include <algorithm>
#include <iostream>

enum class Player : char { X = 'X', O = 'O', Empty = ' ' };

enum class Result : int { X = 1, Y = 2, equal = 0, min = -1, max = 4 };

constexpr size_t cells = 4;

using Board = std::array<std::array<Player, cells>, cells>;

class Util {
public:
  static bool can_move(const Board &);

  static Result who_is_winner(const Board &);

  static Result minimax(Board &board, size_t depth, bool is_max);

public:
  static void print_board(const Board &board);

  static std::pair<int, int> find_optimal_position(Board &board);
};

#endif
